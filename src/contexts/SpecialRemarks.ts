import React from 'react';
import SpecialRemark from '../model/SpecialRemark';

export default React.createContext<SpecialRemark[] | undefined>(undefined);
