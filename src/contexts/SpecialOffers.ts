import React from 'react';
import SpecialOffer from '../model/SpecialOffer';

export default React.createContext<SpecialOffer[] | undefined>(undefined);
