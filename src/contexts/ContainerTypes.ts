import React from 'react';
import ContainerType from '../model/ContainerType';

export default React.createContext<ContainerType[] | undefined>(undefined);
