import React from 'react';
import PortTerm from '../model/PortTerm';

export default React.createContext<PortTerm[] | undefined>(undefined);
