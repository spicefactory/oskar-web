import React from 'react';
import { Tag } from '../model/Tag';

export default React.createContext<Tag[] | undefined>(undefined);
