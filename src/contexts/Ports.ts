import React from 'react';
import Port from '../model/Port';

export default React.createContext<Port[] | undefined>(undefined);
