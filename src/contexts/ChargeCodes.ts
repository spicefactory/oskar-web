import React from 'react';
import ChargeCode from '../model/ChargeCode';

export default React.createContext<ChargeCode[] | undefined>(undefined);
