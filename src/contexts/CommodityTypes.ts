import React from 'react';
import CommodityType from '../model/CommodityType';

export default React.createContext<CommodityType[] | undefined>(undefined);
