import { DateRange } from '../components/daterangepicker/types';
import UserRecord from '../model/UserRecord';
import Client from '../model/Client';
import Port from '../model/Port';
import set from 'lodash/fp/set';
import { subMonths, subWeeks } from 'date-fns';
import { BookingRequestStatusCode } from '../model/BookingRequest';

export type ActionType = 'set' | 'clear';
export type FilterFields =
  | 'dateRange'
  | 'archived'
  | 'category'
  | 'pendingPayment'
  | 'assignee'
  | 'originPort'
  | 'destinationPort'
  | 'clientFilter';

export const INITIAL_DATERANGE_FILTER = {
  startDate: subWeeks(new Date(), 2),
  endDate: new Date(),
};

export const LAST_3_MONTHS = {
  startDate: subMonths(new Date(), 3),
  endDate: new Date(),
};

// filters by which we can filter bookings
export interface ContextFilters {
  dateRange?: DateRange;
  archived: boolean;
  hold: boolean;
  pendingPayment?: boolean;
  assignee?: UserRecord;
  clientFilter?: Client;
  originPort?: Port;
  destinationPort?: Port;
  assignedTags?: string[];
  minStatusCode?: BookingRequestStatusCode;
  maxStatusCode?: BookingRequestStatusCode;
}

export type Action = {
  type: ActionType;
  field: FilterFields;
  value?: DateRange | boolean | string | undefined | Port | Client | UserRecord;
};

export type ActionValues = {};

export const reducer = <T extends ContextFilters>(state: T, action: Action): T => {
  switch (action.type) {
    case 'set':
      return set(action.field, action.value)(state);
    case 'clear':
      return set(action.field, undefined)(state);
    default:
      return state;
  }
};
