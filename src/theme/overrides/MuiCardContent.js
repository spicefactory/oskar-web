import { spacing } from '../spacing';

export default {
  root: {
    padding: spacing(3),
  },
};
