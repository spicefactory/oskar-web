import { spacing } from '../spacing';

export default {
  outlined: {
    '&$shrink': {
      transform: `translate(${spacing(1.25)}px, -6px) scale(0.75)`,
    },
  },
};
