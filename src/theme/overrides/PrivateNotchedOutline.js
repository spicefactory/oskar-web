import { spacing } from '../spacing';

export default {
  root: {
    padding: spacing(1, 0.5),
  },
};
