import { spacing } from '../spacing';

export default {
  input: {
    paddingLeft: spacing(1.25),
    paddingRight: spacing(1.25),
  },
};
