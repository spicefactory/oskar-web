import palette from '../palette';

export default {
  input: {
    '&::placeholder': {
      opacity: 1,
      color: palette.text.secondary,
    },
  },
};
