import React from 'react';

export const Error: React.FC = () => (
  <svg viewBox="0 46 144.54 56">
    <defs>
      <style>
        {
          '.cls-1{fill:none;}.cls-2{fill:#a3c8e7;}.cls-3{fill:#465d7a;}.cls-4{fill:#2c3c50;}.cls-5{fill:#ededed;}.cls-6{fill:#1f2c38;}.cls-7{clip - path:url(#clip-path);}.cls-8{fill:#fff;}.cls-9{fill:#fefefe;}.cls-10{fill:#fdfdfe;}.cls-11{fill:#fcfdfd;}.cls-12{fill:#fbfcfd;}.cls-13{fill:#fafbfc;}.cls-14{fill:#f9fafc;}.cls-15{fill:#f8fafb;}.cls-16{fill:#f7f9fb;}.cls-17{fill:#f6f8fa;}.cls-18{fill:#f5f7fa;}.cls-19{fill:#f4f6f9;}.cls-20{fill:#f3f6f9;}.cls-21{fill:#f2f5f8;}.cls-22{fill:#f1f4f8;}.cls-23{fill:#f0f3f7;}.cls-24{fill:#eff3f7;}.cls-25{fill:#eef2f6;}.cls-26{fill:#edf1f6;}.cls-27{fill:#ecf0f5;}.cls-28{fill:#ebeff4;}.cls-29{fill:#eaeff4;}.cls-30{fill:#e9eef3;}.cls-31{fill:#e8edf3;}.cls-32{fill:#e7ecf2;}.cls-33{fill:#e6ecf2;}.cls-34{fill:#e5ebf1;}.cls-35{fill:#e4eaf1;}.cls-36{fill:#e3e9f0;}.cls-37{fill:#e2e8f0;}.cls-38{fill:#e1e8ef;}.cls-39{fill:#e0e7ef;}.cls-40{fill:#dfe6ee;}.cls-41{fill:#dee5ee;}.cls-42{fill:#dde5ed;}.cls-43{fill:#dce4ed;}.cls-44{fill:#dbe3ec;}.cls-45{fill:#dae2ec;}.cls-46{fill:#d9e1eb;}.cls-47{fill:#d8e1ea;}.cls-48{fill:#d7e0ea;}.cls-49{fill:#d6dfe9;}.cls-50{fill:#d5dee9;}.cls-51{fill:#d4dee8;}.cls-52{fill:#d3dde8;}.cls-53{fill:#d2dce7;}.cls-54{fill:#d1dbe7;}.cls-55{fill:#d0dae6;}.cls-56{fill:#cfdae6;}.cls-57{fill:#ced9e5;}.cls-58{fill:#cdd8e5;}.cls-59{fill:#ccd7e4;}.cls-60{fill:#cbd7e4;}.cls-61{fill:#cad6e3;}.cls-62{fill:#c9d5e3;}.cls-63{fill:#c8d4e2;}.cls-64{fill:#c7d3e2;}.cls-65{fill:#c6d3e1;}.cls-66{fill:#c5d2e0;}.cls-67{fill:#c4d1e0;}.cls-68{fill:#c3d0df;}.cls-69{fill:#c2d0df;}.cls-70{fill:#c1cfde;}.cls-71{fill:#c0cede;}.cls-72{fill:#bfcddd;}.cls-73{fill:#beccdd;}.cls-74{fill:#bdccdc;}.cls-75{fill:#bccbdc;}.cls-76{fill:#bbcadb;}.cls-77{fill:#bac9db;}.cls-78{fill:#b9c9da;}.cls-79{fill:#b8c8da;}.cls-80{fill:#b7c7d9;}.cls-81{fill:#b6c6d9;}.cls-82{fill:#b5c5d8;}.cls-83{fill:#b4c5d8;}.cls-84{fill:#b3c4d7;}.cls-85{fill:#b2c3d6;}.cls-86{fill:#b1c2d6;}.cls-87{fill:#b0c2d5;}.cls-88{fill:#afc1d5;}.cls-89{fill:#aec0d4;}.cls-90{fill:#adbfd4;}.cls-91{fill:#acbed3;}.cls-92{fill:#abbed3;}.cls-93{fill:#aabdd2;}.cls-94{fill:#a8bcd2;}.cls-95{fill:#a7bbd1;}.cls-96{fill:#a6bbd1;}.cls-97{fill:#a5bad0;}.cls-98{fill:#a4b9d0;}.cls-99{fill:#a3b8cf;}.cls-100{fill:#a2b7cf;}.cls-101{fill:#a1b7ce;}.cls-102{fill:#a0b6ce;}.cls-103{fill:#9fb5cd;}.cls-104{fill:#9eb4cc;}.cls-105{fill:#9db4cc;}.cls-106{fill:#9cb3cb;}.cls-107{fill:#9bb2cb;}.cls-108{fill:#9ab1ca;}.cls-109{fill:#99b0ca;}.cls-110{fill:#98b0c9;}.cls-111{fill:#97afc9;}.cls-112{fill:#96aec8;}.cls-113{fill:#95adc8;}.cls-114{fill:#94adc7;}.cls-115{fill:#93acc7;}.cls-116{fill:#92abc6;}.cls-117{fill:#91aac6;}.cls-118{fill:#90a9c5;}.cls-119{fill:#8fa9c5;}.cls-120{fill:#8ea8c4;}.cls-121{fill:#8da7c4;}.cls-122{fill:#8ca6c3;}.cls-123{fill:#8ba6c2;}.cls-124{fill:#8aa5c2;}.cls-125{fill:#89a4c1;}.cls-126{fill:#88a3c1;}.cls-127{fill:#87a2c0;}.cls-128{fill:#86a2c0;}.cls-129{fill:#85a1bf;}.cls-130{fill:#84a0bf;}.cls-131{fill:#839fbe;}.cls-132{fill:#829fbe;}.cls-133{fill:#819ebd;}.cls-134{fill:#809dbd;}.cls-135{fill:#7f9cbc;}.cls-136{fill:#7e9bbc;}.cls-137{fill:#7d9bbb;}.cls-138{fill:#7c9abb;}.cls-139{fill:#7b99ba;}.cls-140{fill:#7a98ba;}.cls-141{fill:#7998b9;}.cls-142{fill:#7897b8;}.cls-143{fill:#7796b8;}.cls-144{fill:#7695b7;}.cls-145{fill:#7594b7;}.cls-146{fill:#7494b6;}.cls-147{fill:#7393b6;}.cls-148{fill:#7292b5;}.cls-149{fill:#7191b5;}.cls-150{fill:#7091b4;}.cls-151{fill:#6f90b4;}.cls-152{fill:#6e8fb3;}.cls-153{fill:#6d8eb3;}.cls-154{fill:#6c8db2;}.cls-155{fill:#6b8db2;}.cls-156{fill:#6a8cb1;}.cls-157{fill:#698bb1;}.cls-158{fill:#688ab0;}.cls-159{fill:#678ab0;}.cls-160{fill:#6689af;}.cls-161{fill:#6588ae;}.cls-162{fill:#6487ae;}.cls-163{fill:#6386ad;}.cls-164{fill:#6286ad;}.cls-165{fill:#6185ac;}.cls-166{fill:#6084ac;}.cls-167{fill:#5f83ab;}.cls-168{fill:#5e83ab;}.cls-169{fill:#5d82aa;}.cls-170{fill:#5c81aa;}.cls-171{fill:#5b80a9;}.cls-172{fill:#5a7fa9;}.cls-173{fill:#597fa8;}.cls-174{fill:#587ea8;}.cls-175{fill:#577da7;}.cls-176{fill:#567ca7;}.cls-177{fill:#557ca6;}.cls-178{fill:#547ba6;}.cls-179{fill:#537aa5;}'
        }
      </style>
      <clipPath id="clip-path" transform="translate(0)">
        <path
          className="cls-1"
          d="M0,100.8s6.44-7.61,12.82-6.37,8.76,3.68,15.69,3.89,8.92-2.71,15.49-3,11.66,3.19,16.56,3,8.77-2.88,14-3.09,10.41,3.08,15.81,2.86,6.48-2.8,11.41-3.66,8.72,4.35,13.07,3.87,10-4.3,12.62-4.3c11,0,11.49,8,14.3,8.63"
        />
      </clipPath>
    </defs>
    <g id="Layer_2" data-name="Layer 2">
      <g id="Layer_1-2" data-name="Layer 1">
        <rect className="cls-1" x="0.04" width="141.73" height="141.73" />
        <rect
          className="cls-2"
          x="60.44"
          y="59.12"
          width="13.1"
          height="13.1"
          transform="translate(-22.34 37.04) rotate(-26.66)"
        />
        <rect className="cls-2" x="72.59" y="74.51" width="13.1" height="13.1" />
        <rect className="cls-3" x="85.69" y="74.51" width="13.1" height="13.1" />
        <rect className="cls-4" x="36.58" y="65.11" width="16.36" height="22.57" />
        <rect className="cls-4" x="27.11" y="74.63" width="9.47" height="13.06" />
        <rect className="cls-3" x="28.51" y="72.91" width="7.03" height="1.72" />
        <rect className="cls-3" x="37.72" y="63.39" width="14.31" height="1.72" />
        <rect className="cls-2" x="29.46" y="76.76" width="1.81" height="7.2" />
        <rect className="cls-2" x="32.79" y="76.76" width="1.81" height="7.2" />
        <rect className="cls-2" x="42.19" y="67.81" width="1.81" height="16.15" />
        <rect className="cls-2" x="45.52" y="67.81" width="1.81" height="16.15" />
        <rect
          className="cls-5"
          x="108.26"
          y="84.95"
          width="13.1"
          height="13.1"
          transform="translate(93.03 -53.69) rotate(42.91)"
        />
        <polygon
          className="cls-2"
          points="105.56 92.05 105.55 91.63 123.91 90.96 127.23 80.59 127.63 80.72 124.22 91.37 105.56 92.05"
        />
        <polygon
          className="cls-2"
          points="114.98 100.81 114.3 82.06 124.1 82.83 124.06 83.25 114.73 82.51 115.4 100.79 114.98 100.81"
        />
        <rect
          className="cls-5"
          x="57.5"
          y="74.94"
          width="13.1"
          height="13.1"
          transform="translate(36.71 -18.49) rotate(22.87)"
        />
        <rect
          className="cls-2"
          x="54.97"
          y="81.28"
          width="18.34"
          height="0.42"
          transform="translate(-25.99 30.2) rotate(-22.15)"
        />
        <rect
          className="cls-2"
          x="63.93"
          y="72.41"
          width="0.42"
          height="18.34"
          transform="translate(-26 30.16) rotate(-22.13)"
        />
        <rect
          className="cls-3"
          x="57.9"
          y="65.47"
          width="18.18"
          height="0.41"
          transform="translate(-16.42 108.62) rotate(-71.67)"
        />
        <rect
          className="cls-2"
          x="107.93"
          y="65.89"
          width="13.1"
          height="13.1"
          transform="translate(73.73 -56.76) rotate(40.17)"
        />
        <polygon
          className="cls-3"
          points="105.53 73.42 105.5 73.01 123.49 71.49 122.53 59.77 122.94 59.73 123.94 71.86 105.53 73.42"
        />
        <polygon className="cls-3" points="72.79 87.76 72.5 87.46 85.4 74.51 85.69 74.8 72.79 87.76" />
        <rect
          className="cls-3"
          x="92.57"
          y="60.98"
          width="13.1"
          height="13.1"
          transform="translate(20.77 -23.28) rotate(14.95)"
        />
        <rect
          className="cls-4"
          x="90.06"
          y="67.34"
          width="18.16"
          height="0.41"
          transform="translate(-20.49 58.67) rotate(-30.02)"
        />
        <rect
          className="cls-4"
          x="98.94"
          y="58.47"
          width="0.41"
          height="18.16"
          transform="translate(-20.5 58.73) rotate(-30.05)"
        />
        <rect
          className="cls-4"
          x="83.01"
          y="80.93"
          width="18.32"
          height="0.42"
          transform="translate(-30.37 88.94) rotate(-45)"
        />
        <polygon
          className="cls-6"
          points="21.68 87.67 66.99 87.67 73.96 79.39 112.31 79.39 100.45 98.32 25.01 98.32 21.68 87.67"
        />
        <circle className="cls-2" cx="35.09" cy="92.02" r="1.4" />
        <circle className="cls-2" cx="44.23" cy="92.02" r="1.4" />
        <circle className="cls-2" cx="53.44" cy="92.02" r="1.4" />
        <circle className="cls-2" cx="63.04" cy="92.02" r="1.4" />
        <circle className="cls-2" cx="72.27" cy="92.02" r="1.4" />
        <circle className="cls-2" cx="81.25" cy="91.93" r="1.4" />
        <circle className="cls-2" cx="90" cy="91.93" r="1.4" />
        <rect
          className="cls-3"
          x="101.53"
          y="48.11"
          width="0.42"
          height="7"
          transform="translate(-12.28 47.85) rotate(-25.01)"
        />
        <rect
          className="cls-3"
          x="104.1"
          y="49.93"
          width="9.59"
          height="0.42"
          transform="translate(32.92 142.87) rotate(-75.39)"
        />
        <rect
          className="cls-3"
          x="111.08"
          y="55.57"
          width="6.36"
          height="0.42"
          transform="translate(-11.78 39) rotate(-18.43)"
        />
        <g className="cls-7">
          <polygon className="cls-8" points="0 100.3 144.54 101.52 144.54 103.23 0 103.23 0 100.3" />
          <polygon className="cls-8" points="0 100.25 144.54 101.48 144.54 101.52 0 100.3 0 100.25" />
          <polygon className="cls-9" points="0 100.21 144.54 101.43 144.54 101.48 0 100.25 0 100.21" />
          <polygon className="cls-10" points="0 100.16 144.54 101.39 144.54 101.43 0 100.21 0 100.16" />
          <polygon className="cls-11" points="0 100.12 144.54 101.34 144.54 101.39 0 100.16 0 100.12" />
          <polygon className="cls-12" points="0 100.08 144.54 101.3 144.54 101.34 0 100.12 0 100.08" />
          <polygon className="cls-13" points="0 100.03 144.54 101.26 144.54 101.3 0 100.08 0 100.03" />
          <polygon className="cls-14" points="0 99.99 144.54 101.21 144.54 101.26 0 100.03 0 99.99" />
          <polygon className="cls-15" points="0 99.94 144.54 101.17 144.54 101.21 0 99.99 0 99.94" />
          <polygon className="cls-16" points="0 99.9 144.54 101.13 144.54 101.17 0 99.94 0 99.9" />
          <polygon className="cls-17" points="0 99.86 144.54 101.08 144.54 101.13 0 99.9 0 99.86" />
          <polygon className="cls-18" points="0 99.81 144.54 101.04 144.54 101.08 0 99.86 0 99.81" />
          <polygon className="cls-19" points="0 99.77 144.54 100.99 144.54 101.04 0 99.81 0 99.77" />
          <polygon className="cls-20" points="0 99.72 144.54 100.95 144.54 100.99 0 99.77 0 99.72" />
          <polygon className="cls-21" points="0 99.68 144.54 100.9 144.54 100.95 0 99.72 0 99.68" />
          <polygon className="cls-22" points="0 99.64 144.54 100.86 144.54 100.9 0 99.68 0 99.64" />
          <polygon className="cls-23" points="0 99.59 144.54 100.82 144.54 100.86 0 99.64 0 99.59" />
          <polygon className="cls-24" points="0 99.55 144.54 100.77 144.54 100.82 0 99.59 0 99.55" />
          <polygon className="cls-25" points="0 99.5 144.54 100.73 144.54 100.77 0 99.55 0 99.5" />
          <polygon className="cls-26" points="0 99.46 144.54 100.68 144.54 100.73 0 99.5 0 99.46" />
          <polygon className="cls-27" points="0 99.41 144.54 100.64 144.54 100.68 0 99.46 0 99.41" />
          <polygon className="cls-28" points="0 99.37 144.54 100.6 144.54 100.64 0 99.41 0 99.37" />
          <polygon className="cls-29" points="0 99.33 144.54 100.55 144.54 100.6 0 99.37 0 99.33" />
          <polygon className="cls-30" points="0 99.28 144.54 100.51 144.54 100.55 0 99.33 0 99.28" />
          <polygon className="cls-31" points="0 99.24 144.54 100.46 144.54 100.51 0 99.28 0 99.24" />
          <polygon className="cls-32" points="0 99.19 144.54 100.42 144.54 100.46 0 99.24 0 99.19" />
          <polygon className="cls-33" points="0 99.15 144.54 100.38 144.54 100.42 0 99.19 0 99.15" />
          <polygon className="cls-34" points="0 99.11 144.54 100.33 144.54 100.38 0 99.15 0 99.11" />
          <polygon className="cls-35" points="0 99.06 144.54 100.29 144.54 100.33 0 99.11 0 99.06" />
          <polygon className="cls-36" points="0 99.02 144.54 100.24 144.54 100.29 0 99.06 0 99.02" />
          <polygon className="cls-37" points="0 98.97 144.54 100.2 144.54 100.24 0 99.02 0 98.97" />
          <polygon className="cls-38" points="0 98.93 144.54 100.16 144.54 100.2 0 98.97 0 98.93" />
          <polygon className="cls-39" points="0 98.89 144.54 100.11 144.54 100.16 0 98.93 0 98.89" />
          <polygon className="cls-40" points="0 98.84 144.54 100.07 144.54 100.11 0 98.89 0 98.84" />
          <polygon className="cls-41" points="0 98.8 144.54 100.02 144.54 100.07 0 98.84 0 98.8" />
          <polygon className="cls-42" points="0 98.75 144.54 99.98 144.54 100.02 0 98.8 0 98.75" />
          <polygon className="cls-43" points="0 98.71 144.54 99.94 144.54 99.98 0 98.75 0 98.71" />
          <polygon className="cls-44" points="0 98.67 144.54 99.89 144.54 99.94 0 98.71 0 98.67" />
          <polygon className="cls-45" points="0 98.62 144.54 99.85 144.54 99.89 0 98.67 0 98.62" />
          <polygon className="cls-46" points="0 98.58 144.54 99.8 144.54 99.85 0 98.62 0 98.58" />
          <polygon className="cls-47" points="0 98.53 144.54 99.76 144.54 99.8 0 98.58 0 98.53" />
          <polygon className="cls-48" points="0 98.49 144.54 99.71 144.54 99.76 0 98.53 0 98.49" />
          <polygon className="cls-49" points="0 98.44 144.54 99.67 144.54 99.71 0 98.49 0 98.44" />
          <polygon className="cls-50" points="0 98.4 144.54 99.63 144.54 99.67 0 98.44 0 98.4" />
          <polygon className="cls-51" points="0 98.36 144.54 99.58 144.54 99.63 0 98.4 0 98.36" />
          <polygon className="cls-52" points="0 98.31 144.54 99.54 144.54 99.58 0 98.36 0 98.31" />
          <polygon className="cls-53" points="0 98.27 144.54 99.49 144.54 99.54 0 98.31 0 98.27" />
          <polygon className="cls-54" points="0 98.22 144.54 99.45 144.54 99.49 0 98.27 0 98.22" />
          <polygon className="cls-55" points="0 98.18 144.54 99.41 144.54 99.45 0 98.22 0 98.18" />
          <polygon className="cls-56" points="0 98.14 144.54 99.36 144.54 99.41 0 98.18 0 98.14" />
          <polygon className="cls-57" points="0 98.09 144.54 99.32 144.54 99.36 0 98.14 0 98.09" />
          <polygon className="cls-58" points="0 98.05 144.54 99.27 144.54 99.32 0 98.09 0 98.05" />
          <polygon className="cls-59" points="0 98 144.54 99.23 144.54 99.27 0 98.05 0 98" />
          <polygon className="cls-60" points="0 97.96 144.54 99.19 144.54 99.23 0 98 0 97.96" />
          <polygon className="cls-61" points="0 97.92 144.54 99.14 144.54 99.19 0 97.96 0 97.92" />
          <polygon className="cls-62" points="0 97.87 144.54 99.1 144.54 99.14 0 97.92 0 97.87" />
          <polygon className="cls-63" points="0 97.83 144.54 99.05 144.54 99.1 0 97.87 0 97.83" />
          <polygon className="cls-64" points="0 97.78 144.54 99.01 144.54 99.05 0 97.83 0 97.78" />
          <polygon className="cls-65" points="0 97.74 144.54 98.97 144.54 99.01 0 97.78 0 97.74" />
          <polygon className="cls-66" points="0 97.69 144.54 98.92 144.54 98.97 0 97.74 0 97.69" />
          <polygon className="cls-67" points="0 97.65 144.54 98.88 144.54 98.92 0 97.69 0 97.65" />
          <polygon className="cls-68" points="0 97.61 144.54 98.83 144.54 98.88 0 97.65 0 97.61" />
          <polygon className="cls-69" points="0 97.56 144.54 98.79 144.54 98.83 0 97.61 0 97.56" />
          <polygon className="cls-70" points="0 97.52 144.54 98.74 144.54 98.79 0 97.56 0 97.52" />
          <polygon className="cls-71" points="0 97.47 144.54 98.7 144.54 98.74 0 97.52 0 97.47" />
          <polygon className="cls-72" points="0 97.43 144.54 98.66 144.54 98.7 0 97.47 0 97.43" />
          <polygon className="cls-73" points="0 97.39 144.54 98.61 144.54 98.66 0 97.43 0 97.39" />
          <polygon className="cls-74" points="0 97.34 144.54 98.57 144.54 98.61 0 97.39 0 97.34" />
          <polygon className="cls-75" points="0 97.3 144.54 98.52 144.54 98.57 0 97.34 0 97.3" />
          <polygon className="cls-76" points="0 97.25 144.54 98.48 144.54 98.52 0 97.3 0 97.25" />
          <polygon className="cls-77" points="0 97.21 144.54 98.44 144.54 98.48 0 97.25 0 97.21" />
          <polygon className="cls-78" points="0 97.17 144.54 98.39 144.54 98.44 0 97.21 0 97.17" />
          <polygon className="cls-79" points="0 97.12 144.54 98.35 144.54 98.39 0 97.17 0 97.12" />
          <polygon className="cls-80" points="0 97.08 144.54 98.3 144.54 98.35 0 97.12 0 97.08" />
          <polygon className="cls-81" points="0 97.03 144.54 98.26 144.54 98.3 0 97.08 0 97.03" />
          <polygon className="cls-82" points="0 96.99 144.54 98.22 144.54 98.26 0 97.03 0 96.99" />
          <polygon className="cls-83" points="0 96.95 144.54 98.17 144.54 98.22 0 96.99 0 96.95" />
          <polygon className="cls-84" points="0 96.9 144.54 98.13 144.54 98.17 0 96.95 0 96.9" />
          <polygon className="cls-85" points="0 96.86 144.54 98.08 144.54 98.13 0 96.9 0 96.86" />
          <polygon className="cls-86" points="0 96.81 144.54 98.04 144.54 98.08 0 96.86 0 96.81" />
          <polygon className="cls-87" points="0 96.77 144.54 98 144.54 98.04 0 96.81 0 96.77" />
          <polygon className="cls-88" points="0 96.72 144.54 97.95 144.54 98 0 96.77 0 96.72" />
          <polygon className="cls-89" points="0 96.68 144.54 97.91 144.54 97.95 0 96.72 0 96.68" />
          <polygon className="cls-90" points="0 96.64 144.54 97.86 144.54 97.91 0 96.68 0 96.64" />
          <polygon className="cls-91" points="0 96.59 144.54 97.82 144.54 97.86 0 96.64 0 96.59" />
          <polygon className="cls-92" points="0 96.55 144.54 97.78 144.54 97.82 0 96.59 0 96.55" />
          <polygon className="cls-93" points="0 96.5 144.54 97.73 144.54 97.78 0 96.55 0 96.5" />
          <polygon className="cls-94" points="0 96.46 144.54 97.69 144.54 97.73 0 96.5 0 96.46" />
          <polygon className="cls-95" points="0 96.42 144.54 97.64 144.54 97.69 0 96.46 0 96.42" />
          <polygon className="cls-96" points="0 96.37 144.54 97.6 144.54 97.64 0 96.42 0 96.37" />
          <polygon className="cls-97" points="0 96.33 144.54 97.55 144.54 97.6 0 96.37 0 96.33" />
          <polygon className="cls-98" points="0 96.28 144.54 97.51 144.54 97.55 0 96.33 0 96.28" />
          <polygon className="cls-99" points="0 96.24 144.54 97.47 144.54 97.51 0 96.28 0 96.24" />
          <polygon className="cls-100" points="0 96.2 144.54 97.42 144.54 97.47 0 96.24 0 96.2" />
          <polygon className="cls-101" points="0 96.15 144.54 97.38 144.54 97.42 0 96.2 0 96.15" />
          <polygon className="cls-102" points="0 96.11 144.54 97.33 144.54 97.38 0 96.15 0 96.11" />
          <polygon className="cls-103" points="0 96.06 144.54 97.29 144.54 97.33 0 96.11 0 96.06" />
          <polygon className="cls-104" points="0 96.02 144.54 97.25 144.54 97.29 0 96.06 0 96.02" />
          <polygon className="cls-105" points="0 95.98 144.54 97.2 144.54 97.25 0 96.02 0 95.98" />
          <polygon className="cls-106" points="0 95.93 144.54 97.16 144.54 97.2 0 95.98 0 95.93" />
          <polygon className="cls-107" points="0 95.89 144.54 97.11 144.54 97.16 0 95.93 0 95.89" />
          <polygon className="cls-108" points="0 95.84 144.54 97.07 144.54 97.11 0 95.89 0 95.84" />
          <polygon className="cls-109" points="0 95.8 144.54 97.03 144.54 97.07 0 95.84 0 95.8" />
          <polygon className="cls-110" points="0 95.75 144.54 96.98 144.54 97.03 0 95.8 0 95.75" />
          <polygon className="cls-111" points="0 95.71 144.54 96.94 144.54 96.98 0 95.75 0 95.71" />
          <polygon className="cls-112" points="0 95.67 144.54 96.89 144.54 96.94 0 95.71 0 95.67" />
          <polygon className="cls-113" points="0 95.62 144.54 96.85 144.54 96.89 0 95.67 0 95.62" />
          <polygon className="cls-114" points="0 95.58 144.54 96.81 144.54 96.85 0 95.62 0 95.58" />
          <polygon className="cls-115" points="0 95.53 144.54 96.76 144.54 96.81 0 95.58 0 95.53" />
          <polygon className="cls-116" points="0 95.49 144.54 96.72 144.54 96.76 0 95.53 0 95.49" />
          <polygon className="cls-117" points="0 95.45 144.54 96.67 144.54 96.72 0 95.49 0 95.45" />
          <polygon className="cls-118" points="0 95.4 144.54 96.63 144.54 96.67 0 95.45 0 95.4" />
          <polygon className="cls-119" points="0 95.36 144.54 96.58 144.54 96.63 0 95.4 0 95.36" />
          <polygon className="cls-120" points="0 95.31 144.54 96.54 144.54 96.58 0 95.36 0 95.31" />
          <polygon className="cls-121" points="0 95.27 144.54 96.5 144.54 96.54 0 95.31 0 95.27" />
          <polygon className="cls-122" points="0 95.23 144.54 96.45 144.54 96.5 0 95.27 0 95.23" />
          <polygon className="cls-123" points="0 95.18 144.54 96.41 144.54 96.45 0 95.23 0 95.18" />
          <polygon className="cls-124" points="0 95.14 144.54 96.36 144.54 96.41 0 95.18 0 95.14" />
          <polygon className="cls-125" points="0 95.09 144.54 96.32 144.54 96.36 0 95.14 0 95.09" />
          <polygon className="cls-126" points="0 95.05 144.54 96.28 144.54 96.32 0 95.09 0 95.05" />
          <polygon className="cls-127" points="0 95.01 144.54 96.23 144.54 96.28 0 95.05 0 95.01" />
          <polygon className="cls-128" points="0 94.96 144.54 96.19 144.54 96.23 0 95.01 0 94.96" />
          <polygon className="cls-129" points="0 94.92 144.54 96.14 144.54 96.19 0 94.96 0 94.92" />
          <polygon className="cls-130" points="0 94.87 144.54 96.1 144.54 96.14 0 94.92 0 94.87" />
          <polygon className="cls-131" points="0 94.83 144.54 96.06 144.54 96.1 0 94.87 0 94.83" />
          <polygon className="cls-132" points="0 94.79 144.54 96.01 144.54 96.06 0 94.83 0 94.79" />
          <polygon className="cls-133" points="0 94.74 144.54 95.97 144.54 96.01 0 94.79 0 94.74" />
          <polygon className="cls-134" points="0 94.7 144.54 95.92 144.54 95.97 0 94.74 0 94.7" />
          <polygon className="cls-135" points="0 94.65 144.54 95.88 144.54 95.92 0 94.7 0 94.65" />
          <polygon className="cls-136" points="0 94.61 144.54 95.83 144.54 95.88 0 94.65 0 94.61" />
          <polygon className="cls-137" points="0 94.56 144.54 95.79 144.54 95.83 0 94.61 0 94.56" />
          <polygon className="cls-138" points="0 94.52 144.54 95.75 144.54 95.79 0 94.56 0 94.52" />
          <polygon className="cls-139" points="0 94.48 144.54 95.7 144.54 95.75 0 94.52 0 94.48" />
          <polygon className="cls-140" points="0 94.43 144.54 95.66 144.54 95.7 0 94.48 0 94.43" />
          <polygon className="cls-141" points="0 94.39 144.54 95.61 144.54 95.66 0 94.43 0 94.39" />
          <polygon className="cls-142" points="0 94.34 144.54 95.57 144.54 95.61 0 94.39 0 94.34" />
          <polygon className="cls-143" points="0 94.3 144.54 95.53 144.54 95.57 0 94.34 0 94.3" />
          <polygon className="cls-144" points="0 94.26 144.54 95.48 144.54 95.53 0 94.3 0 94.26" />
          <polygon className="cls-145" points="0 94.21 144.54 95.44 144.54 95.48 0 94.26 0 94.21" />
          <polygon className="cls-146" points="0 94.17 144.54 95.39 144.54 95.44 0 94.21 0 94.17" />
          <polygon className="cls-147" points="0 94.12 144.54 95.35 144.54 95.39 0 94.17 0 94.12" />
          <polygon className="cls-148" points="0 94.08 144.54 95.31 144.54 95.35 0 94.12 0 94.08" />
          <polygon className="cls-149" points="0 94.04 144.54 95.26 144.54 95.31 0 94.08 0 94.04" />
          <polygon className="cls-150" points="0 93.99 144.54 95.22 144.54 95.26 0 94.04 0 93.99" />
          <polygon className="cls-151" points="0 93.95 144.54 95.17 144.54 95.22 0 93.99 0 93.95" />
          <polygon className="cls-152" points="0 93.9 144.54 95.13 144.54 95.17 0 93.95 0 93.9" />
          <polygon className="cls-153" points="0 93.86 144.54 95.09 144.54 95.13 0 93.9 0 93.86" />
          <polygon className="cls-154" points="0 93.82 144.54 95.04 144.54 95.09 0 93.86 0 93.82" />
          <polygon className="cls-155" points="0 93.77 144.54 95 144.54 95.04 0 93.82 0 93.77" />
          <polygon className="cls-156" points="0 93.73 144.54 94.95 144.54 95 0 93.77 0 93.73" />
          <polygon className="cls-157" points="0 93.68 144.54 94.91 144.54 94.95 0 93.73 0 93.68" />
          <polygon className="cls-158" points="0 93.64 144.54 94.86 144.54 94.91 0 93.68 0 93.64" />
          <polygon className="cls-159" points="0 93.59 144.54 94.82 144.54 94.86 0 93.64 0 93.59" />
          <polygon className="cls-160" points="0 93.55 144.54 94.78 144.54 94.82 0 93.59 0 93.55" />
          <polygon className="cls-161" points="0 93.51 144.54 94.73 144.54 94.78 0 93.55 0 93.51" />
          <polygon className="cls-162" points="0 93.46 144.54 94.69 144.54 94.73 0 93.51 0 93.46" />
          <polygon className="cls-163" points="0 93.42 144.54 94.64 144.54 94.69 0 93.46 0 93.42" />
          <polygon className="cls-164" points="0 93.38 144.54 94.6 144.54 94.64 0 93.42 0 93.38" />
          <polygon className="cls-165" points="0 93.33 144.54 94.56 144.54 94.6 0 93.38 0 93.33" />
          <polygon className="cls-166" points="0 93.29 144.54 94.51 144.54 94.56 0 93.33 0 93.29" />
          <polygon className="cls-167" points="0 93.24 144.54 94.47 144.54 94.51 0 93.29 0 93.24" />
          <polygon className="cls-168" points="0 93.2 144.54 94.42 144.54 94.47 0 93.24 0 93.2" />
          <polygon className="cls-169" points="4.5 93.19 144.54 94.38 144.54 94.42 0 93.2 0 93.19 4.5 93.19" />
          <polygon className="cls-170" points="9.7 93.19 144.54 94.34 144.54 94.38 4.5 93.19 9.7 93.19" />
          <polygon className="cls-171" points="14.9 93.19 144.54 94.29 144.54 94.34 9.7 93.19 14.9 93.19" />
          <polygon className="cls-172" points="20.09 93.19 144.54 94.25 144.54 94.29 14.9 93.19 20.09 93.19" />
          <polygon className="cls-173" points="25.29 93.19 144.54 94.2 144.54 94.25 20.09 93.19 25.29 93.19" />
          <polygon className="cls-174" points="30.49 93.19 144.54 94.16 144.54 94.2 25.29 93.19 30.49 93.19" />
          <polygon className="cls-175" points="35.69 93.19 144.54 94.12 144.54 94.16 30.49 93.19 35.69 93.19" />
          <polygon className="cls-176" points="40.88 93.19 144.54 94.07 144.54 94.12 35.69 93.19 40.88 93.19" />
          <polygon className="cls-177" points="46.08 93.19 144.54 94.03 144.54 94.07 40.88 93.19 46.08 93.19" />
          <polygon className="cls-178" points="51.28 93.19 144.54 93.98 144.54 94.03 46.08 93.19 51.28 93.19" />
          <polygon className="cls-179" points="56.48 93.19 144.54 93.94 144.54 93.98 51.28 93.19 56.48 93.19" />
          <polygon className="cls-179" points="144.54 93.94 56.48 93.19 144.54 93.19 144.54 93.94" />
        </g>
      </g>
    </g>
  </svg>
);
