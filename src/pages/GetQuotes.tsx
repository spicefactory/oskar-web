import React from 'react';
import GetQuotesForm from '../components/GetQuotes';

const GetQuotes: React.FC = () => <GetQuotesForm />;

export default GetQuotes;
