export default interface Client {
  id: string;
  name: string;
  nameSup?: string;
  poBox?: string;
  city: string;
  zip?: string;
  countryCode: string;
}
