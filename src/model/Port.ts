export default interface Port {
  id: string;
  city: string;
  country: string;
}
