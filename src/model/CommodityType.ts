export default interface CommodityType {
  id: string;
  name: string;
}
