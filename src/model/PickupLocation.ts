export default interface PickupLocation {
  id: string;
  city: string;
  countryCode: string;
  name: string;
  nameSup?: string;
  poBox?: string;
  street: string;
  zip: string;
}
