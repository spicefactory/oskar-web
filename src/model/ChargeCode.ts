export default interface ChargeCode {
  id: string;
  text?: string;
  internal1?: string;
  chargeCodeId?: string;
  language?: string;
}
