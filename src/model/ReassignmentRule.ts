export interface ReassignmentRule {
  id: string;
  agentId: string;
  carrier: string;
  reassignedClientId: string;
}
