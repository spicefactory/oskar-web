export default interface OOG {
  width: string;
  height: string;
  length: string;
  weight: string;
  diffWidth?: string;
  diffLength?: string;
  diffHeight?: string;
  diffWeight?: string;
  displacement?: number;
}
