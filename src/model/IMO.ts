export default interface IMO {
  IMOClass: string;
  UNNumber: string;
  PGNumber: string;
}
