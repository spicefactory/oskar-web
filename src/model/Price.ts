import { Currency } from './Payment';

interface Price {
  value: number;
  currency: Currency;
}

export default Price;
