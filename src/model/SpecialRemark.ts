export default interface SpecialRemark {
  id: string;
  text?: string;
}
