import Country from '../Country';
import City from '../City';

interface Destination {
  country: Country;
  city: City;
}
export default Destination;
