export default interface Country {
  name: string;
  countryCode: string;
}
