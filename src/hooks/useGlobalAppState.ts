import { useContext } from 'react';
import { GlobalContext } from '../store/GlobalStore';

export default () => useContext(GlobalContext);
